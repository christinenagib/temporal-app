package com.example.starter.temporal.workers;

import com.example.starter.temporal.workflows.ConvertWorkflowImpl;
import com.example.starter.temporal.activities.InvertConversionActivitiesImpl;
import com.example.starter.temporal.activities.ConversionActivitiesImpl;
import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import io.temporal.client.ActivityCompletionClient;

public class ConvertCurrencyWorker {
  public static void startWorker() {
    WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();

    WorkflowClient client = WorkflowClient.newInstance(service);

    ActivityCompletionClient completionClient = client.newActivityCompletionClient();

    WorkerFactory factory = WorkerFactory.newInstance(client);

    Worker worker = factory.newWorker(Shared.CONVERT_TASK_QUEUE);

    worker.registerWorkflowImplementationTypes(ConvertWorkflowImpl.class);

    worker.registerActivitiesImplementations(new ConversionActivitiesImpl(completionClient), new InvertConversionActivitiesImpl());

    factory.start();

  }
}
