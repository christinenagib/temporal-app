package com.example.starter.temporal.activities;

import io.temporal.activity.ActivityInterface;

@ActivityInterface
public interface ConversionActivities {
  float getResult(String fromCurrency, String toCurrency, float amount);
}
