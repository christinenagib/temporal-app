package com.example.starter.temporal.activities;

import io.temporal.activity.*;
import io.temporal.client.ActivityCompletionClient;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.*;

public class ConversionActivitiesImpl implements ConversionActivities {
  final ActivityCompletionClient completionClient;

  public ConversionActivitiesImpl(ActivityCompletionClient completionClient) {
    this.completionClient = completionClient;
  }

  @Override
  public float getResult(String fromCurrency, String toCurrency, float amount) {


    Vertx vertx = Vertx.vertx();
    WebClient client = WebClient.create(vertx);
    ActivityExecutionContext context = Activity.getExecutionContext();
    byte[] taskToken = context.getTaskToken();

    client
      .get(80, "api.apilayer.com", "https://api.apilayer.com/currency_data/convert?to=" + toCurrency + "&from=" + fromCurrency + "&amount=" + amount)
      .putHeader("apikey", "m62JZp6QKN05VIUj2Fz6jymiRvpCcPc9")
      .send()
      .onSuccess(response -> {
        String result = response.body().toString();
        System.out.println("response : " + response.body().toString());
        JsonObject object = new JsonObject(result);
        System.out.println("get the result");
        float conversionResult = object.getFloat("result");
        System.out.println("Received response with status code " + response.statusCode() + " the result is " + conversionResult);
        System.out.println("conversion result is " + conversionResult);

        completionClient.complete(taskToken, conversionResult);

      })
      .onFailure(err -> {
        System.out.println("Something went wrong " + err.getMessage());
        completionClient.completeExceptionally(taskToken, new Exception(err.getMessage()));
      });

    context.doNotCompleteOnReturn();
    return 0.0f;
  }
}

