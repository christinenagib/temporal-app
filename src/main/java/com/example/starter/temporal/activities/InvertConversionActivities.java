package com.example.starter.temporal.activities;

import io.temporal.activity.ActivityInterface;

@ActivityInterface
public interface InvertConversionActivities {
  public String getRate(float amount, float result);
}
