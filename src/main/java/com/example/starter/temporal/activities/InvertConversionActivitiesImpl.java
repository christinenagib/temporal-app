package com.example.starter.temporal.activities;

public class InvertConversionActivitiesImpl implements InvertConversionActivities {
  @Override
  public String getRate(float amount, float result) {
    float invertedConversion = result / amount;
    return "First conversion was " + result + " and the inverted conversion was " + invertedConversion;
  }
}
