package com.example.starter.temporal.workflows;

import com.example.starter.temporal.activities.InvertConversionActivities;
import com.example.starter.temporal.activities.ConversionActivities;
import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;

import java.time.Duration;

public class ConvertWorkflowImpl implements ConvertWorkflow {

  ActivityOptions options = ActivityOptions.newBuilder()
    .setStartToCloseTimeout(Duration.ofSeconds(60))
    .build();

  private final ConversionActivities conversionActivity = Workflow.newActivityStub(ConversionActivities.class, options);
  private final InvertConversionActivities invertActivity = Workflow.newActivityStub(InvertConversionActivities.class, options);

  @Override
  public String getConversion(String fromCurrency, String toCurrency, float amount) {

    try {
      float currencyResult = conversionActivity.getResult(fromCurrency, toCurrency, amount);

      return invertActivity.getRate(amount, currencyResult);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
