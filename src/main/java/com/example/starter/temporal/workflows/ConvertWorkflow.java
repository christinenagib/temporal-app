package com.example.starter.temporal.workflows;

import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;


@WorkflowInterface
public interface ConvertWorkflow {

  /**
   * This is the method that is executed when the Workflow Execution is started. The Workflow
   * Execution completes when this method finishes execution.
   */
  @WorkflowMethod
  String getConversion(String fromCurrency, String toCurrency, float amount);
}
