package com.example.starter;

import com.example.starter.temporal.workers.ConvertCurrencyWorker;
import com.example.starter.verticle.ClientVerticle;
import io.vertx.core.Vertx;

import java.io.IOException;

public class MainVerticle {

  public static void main(String[] args) throws IOException {
    Vertx vertx = Vertx.vertx();
    ConvertCurrencyWorker.startWorker();
    vertx.deployVerticle(new ClientVerticle());
  }

}
