package com.example.starter.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class RecieverVerticle extends AbstractVerticle {


  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    vertx.eventBus().consumer("message", message -> {
      System.out.println("I have received a message: " + message.body());
      message.reply("replied");
    });

  }
}
