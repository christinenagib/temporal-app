package com.example.starter.verticle;

import com.example.starter.temporal.workflows.ConvertWorkflow;
import io.temporal.api.common.v1.WorkflowExecution;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;

public class ClientVerticle extends AbstractVerticle {

  public String fromCurrency, toCurrency;
  float amount;


  @Override
  public void start(Promise<Void> startPromise) {

    Router router = Router.router(vertx);

    router.route().handler(context -> {

      MultiMap queryParams = context.queryParams();
      fromCurrency = queryParams.contains("fromCurrency") ? queryParams.get("fromCurrency") : "unknown";
      toCurrency = queryParams.contains("toCurrency") ? queryParams.get("toCurrency") : "unknown";
      amount = Float.parseFloat(queryParams.contains("amount") ? queryParams.get("amount") : "0.0");

      WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();

      WorkflowClient client = WorkflowClient.newInstance(service);

      String taskQueue = "ConvertCurrencyTaskQueue";

      WorkflowOptions options = WorkflowOptions.newBuilder()
        .setTaskQueue(taskQueue)
        .build();

      ConvertWorkflow workflow = client.newWorkflowStub(ConvertWorkflow.class, options);

      WorkflowExecution we = WorkflowClient.start(workflow::getConversion, fromCurrency, toCurrency, amount);

      context.json(
        new JsonObject()
          .put("fromCurrency", fromCurrency)
          .put("toCurrency", toCurrency)
          .put("amount", amount)
      );
    });

    vertx.createHttpServer().requestHandler(router)
      .listen(9000)
      .onSuccess(server ->
        System.out.println(
          "HTTP server started on port " + server.actualPort()
        )
      );

  }

}
